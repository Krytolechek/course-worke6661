package sample;

import javax.swing.*;

class Queue {
    private int maxSize;
    private long[] queArray;
    private int front;
    private int rear;
    private int nItems;

    //----------------------------
    public Queue(int s)                      //Конструктор
    {
        maxSize = s;
        queArray = new long[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    //-------------------------------
    public void insert(long j)//Вставка элемента в очередь
    {
        if ((rear == maxSize - 1) && (nItems == maxSize)) {        //Циклический перенос
            rear = -1;
            queArray[++rear] = j;
            front++;//Увеличение rear и вставка
        } else              //Увелиение количества элементов
            if (nItems == maxSize) {               //Циклический перенос
                queArray[++rear] = j;
                front++;
            } else if (rear == maxSize - 1) {               //Циклический перенос
                rear = -1;
                queArray[++rear] = j;                //Увеличение rear и вставка
                nItems++;

            } else {
                queArray[++rear] = j;                //Увеличение rear и вставка
                nItems++;

            }
    }


    //--------------------------------
    void delete()                           //Удаление элемента в начале очереди
    {
        if (nItems == 0) {
            JOptionPane.showMessageDialog(null, "Очередь пуста");
            front = 0;
            rear = -1;
        } else {
            if (nItems == 1) {
                nItems--;
                front = 0;
                rear = -1;
            } else {
                if (front == maxSize || front > maxSize)           //Циклический перенос
                    front = 0;
                int temp = (int) queArray[front++]; //Выборка и увеличение front
                if (front == maxSize || front > maxSize)           //Циклический перенос
                    front = 0;
                nItems--;
            }
        }
    }

    long remove() {
        if (front == maxSize || front > maxSize)           //Циклический перенос
            front = 0;
        long temp = queArray[front++]; //Выборка и увеличение front
        if (front == maxSize || front > maxSize)           //Циклический перенос
            front = 0;
        nItems--;
        return temp;
    }


    public boolean isEmpty()           //true если очередь пуста
    {
        return ((nItems) == 0);
    }


    //---------------------------------
    public int size()                  //Количество элементов в очереди
    {
        return nItems;
    }

    public int vosvratcol(int j) {
        nItems = j;
        return nItems;
    }

    public int vosvratfront() {
        return front;
    }

    public void vosvratfront2(int p) {
        front = p;
    }
}

